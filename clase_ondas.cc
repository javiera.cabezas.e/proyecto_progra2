#include "clase_ondas.hh"
#include <sstream>
#include <iomanip>

onda2d::onda2d( double x_min1, double x_max1, double dm_x1, double t1, double dt1){
	t = t1;
	x_min =x_min1;
	x_max =x_max1;
	dt =dt1;
	dx=dm_x1;
	nm_x=((x_max-x_min)/dx) +  1;
	nm_t=(t/dt) +1;
	grilla = matrix(nm_x,nm_t);}


vector<double> onda2d::itox(int i, int jtongo){
vector<double>ep;
ep.push_back(x_min + i*nm_x);
return ep;
}

double onda2d::itot(int t){
double ep= t*dt;
return ep;

}



void onda2d::condicion_i(double (*f)(vector<double>)){ //condicion inicial
	for (int i=0; i<nm_x; i++){
		grilla(i, 0) = f(itox(i,0));
		grilla(i, 1) = f(itox(i,0));}}

void onda2d::calcula(double v){
	for (int j=0; j<nm_t; j++){
		for(int i=0; i<nm_x; i++){	
		double j1 = (j > 0) ? 2*grilla[i][j-1] : 2*grilla[i][j];
		double j2 = (j > 1) ? grilla[i][j-2] : grilla[i][j];
		double ji1 = (j > 0) ? grilla[i+1][j-1] : grilla[i+1][j];
		double i1 = (i > 0 && j >0) ? grilla[i-1][j-1] : grilla[i][j];
		double coef = j1-j2+v*v*(ji1-j1+i1);
		grilla(i,j,coef);}}}

void onda2d::datos_arch(){ //escribir archivo
	ofstream arch_salida("salidat_f.dat");
	for (int i=0; i < nm_x; i++){
for(int j=0; j<nm_t; j++){
	arch_salida<<itox(i)[0]<<"\t"<<itot(j)<<"\t"<<grilla(i,j)<<endl;}
}}



void onda2d::datos_t(){ // escribimos los datos en muchos archivos
string carpeta("datos_t/");
  string base(".dat");
  for (int j=1;j<=nm_t;j++){
    stringstream ss;
    ss << std::setfill('0') << std::setw(3) << j << base;
    ofstream file( carpeta+ss.str() );
    for (int i=1;i<=nm_x;i++){
	vector<double> p = {itox(i)[0],itot(j)};
	file << p[0] << " " << p[1] << " " << grilla[i][j]<<endl;
      }
    //file << endl;
  }
}





