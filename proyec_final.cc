#include "matriz.h"
#include <fstream>
#include "clase_ondas.hh"

using namespace std; 

double fun_in(vector<double> x){
return x[0]*sin(x[0]*x[0]);}

int main(){

onda2d o(0, 10, 10e-4, 10, 10e-4);
cout<<"paso 1"<<endl;
o.condicion_i(fun_in);
cout<<"paso 2"<<endl;
o.calcula();
cout<<"paso 3"<<endl;
o.datos_t();
cout<<"paso 4"<<endl;
	return 0;
}
