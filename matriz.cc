#include"matriz.h"

ostream& operator<<(ostream&os, matrix a){
    for ( int i = 0; i < a.fila(); i++ ) {
      for ( int j = 0; j < a.col(); j++ ){
	if(a[i][j]== 0){os<<scientific<<setprecision(3)<< a[i][j] <<" \t";}
      if(a[i][j]!= 0){os<<scientific<<setprecision(3)<< a[i][j]<<"\t";};}
    cout<<"\n";}
  return os;}

ostream& operator<<(ostream&os, vector<double>a){
  for (unsigned int i = 0; i < a.size(); ++i){os<<a[i]<<" ";};
  return os;}

matrix::matrix(int a,int b){
    n_fila=a;
    n_col=b;
    vector<vector<double>>m_coef(a,vector<double>(b));
    coef = m_coef;}

matrix::matrix(){}

int matrix::fila(){
    return n_fila;}

int matrix::col(){
    return n_col;}

matrix matrix::operator=(matrix p){
	if(this!=&p) //this es puntero especial que apunta a la izq
	{
		n_fila=p.n_fila;
		n_col=p.n_col;
		for(int i=0; i<n_fila; i++){
		for(int j=0; j<n_col; j++){
		coef[i][j]=p.coef[i][j];}}
	}
	return *this;}

double matrix::operator()(int a, int b, double p){
		return coef[a][b]=p;}

double& matrix::operator()(int a, int b){
return coef[a][b];}

vector<double> matrix::operator[](int b){
return coef[b];}

