#include <iostream>
#include "matriz.h"
#include <fstream>

using namespace std;

class onda2d{
private:
	matrix grilla;
	int nm_x, nm_t;
	double dx;
	double x_min, x_max;
	double t, dt;
	vector<double> itox(int, int y=0); //de matriz a espacio
	double itot(int);
	//double (*funcion)(double); //potencial
public:
	onda2d(double, double, double, double, double);
	void condicion_i(double (*)(vector<double>));
	void calcula(double v=2);
	void datos_arch();
	void datos_t();
};










