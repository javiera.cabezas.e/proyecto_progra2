#ifndef MATRIZ_HH
#define MATRIZ_HH
#include<iostream>
#include<cmath>
#include<vector>
#include<iomanip>

using namespace std;

class matrix{
    private:
    int n_fila,n_col;
    vector<vector<double> >coef;
    public:
    matrix(int, int);
matrix();
    int fila();
    int col();
    matrix operator=(matrix);
    double operator()(int, int, double);
    double& operator()(int, int);
    vector<double> operator[](int);
};


#endif

